/**
 * @file LcdHandler.c
 * @brief From Example application for the LCD1602 16x2 Character Dot Matrix LCD display via I2C backpack..
 */

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "driver/gpio.h"
#include "driver/i2c.h"
#include "esp_log.h"
#include "sdkconfig.h"
#include "rom/uart.h"
#include <string.h>

#include "smbus.h"
#include "i2c-lcd1602.h"
#include "LcdHandler.h"

void i2c_master_init(void)
{
    int i2c_master_port = I2C_MASTER_NUM;
    i2c_config_t conf={0};
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = I2C_MASTER_SDA_IO;
    conf.sda_pullup_en = GPIO_PULLUP_DISABLE;  // GY-2561 provides 10kΩ pullups
    conf.scl_io_num = I2C_MASTER_SCL_IO;
    conf.scl_pullup_en = GPIO_PULLUP_DISABLE;  // GY-2561 provides 10kΩ pullups
    conf.master.clk_speed = I2C_MASTER_FREQ_HZ;
    i2c_param_config(i2c_master_port, &conf);
    i2c_driver_install(i2c_master_port, conf.mode,
                       I2C_MASTER_RX_BUF_LEN,
                       I2C_MASTER_TX_BUF_LEN, 0);
}

void initializeLcd(){
    // Set up I2C
    i2c_master_init();
    i2c_port_t i2c_num = I2C_MASTER_NUM;
    uint8_t address = CONFIG_LCD1602_I2C_ADDRESS;

    // Set up the SMBus
    smbus_info_t * smbus_info = smbus_malloc();
    ESP_ERROR_CHECK(smbus_init(smbus_info, i2c_num, address));
    ESP_ERROR_CHECK(smbus_set_timeout(smbus_info, 1000 / portTICK_RATE_MS));

    // Set up the LCD1602 device with backlight off
    lcd_info = i2c_lcd1602_malloc();
    ESP_ERROR_CHECK(i2c_lcd1602_init(lcd_info, smbus_info, true,
                                     LCD_NUM_ROWS, LCD_NUM_COLUMNS, LCD_NUM_VISIBLE_COLUMNS));

    ESP_ERROR_CHECK(i2c_lcd1602_reset(lcd_info));

    // turn off backlight
    ESP_LOGI(TAG, "backlight off");
    vTaskDelay(1000 / portTICK_RATE_MS);
    i2c_lcd1602_set_backlight(lcd_info, false);

    // turn on backlight
    ESP_LOGI(TAG, "backlight on");
    vTaskDelay(1000 / portTICK_RATE_MS);
    i2c_lcd1602_set_backlight(lcd_info, true);

    ESP_LOGI(TAG, "cursor on");
    vTaskDelay(1000 / portTICK_RATE_MS);
    i2c_lcd1602_set_cursor(lcd_info, true);

    ESP_LOGI(TAG, "start blinking");
    i2c_lcd1602_set_blink(lcd_info, true);
}

void lcd1602_task(void * pvParameter)
{
    lcdPrintedText_t lcdPrintedText=*(lcdPrintedText_t *)pvParameter;

    i2c_lcd1602_move_cursor(lcd_info,lcdPrintedText.row, lcdPrintedText.col);    
    i2c_lcd1602_write_string(lcd_info, (const char *)lcdPrintedText.str);
    
    vTaskDelete(NULL);
}

void writeToLCD(uint8_t row,uint8_t col,char text[20]){
    lcdPrintedText_t lcdPrintedText;
    lcdPrintedText.row=row;
    lcdPrintedText.col=col;
    strcpy(lcdPrintedText.str,text);
    
    xTaskCreate(&lcd1602_task, "lcd1602_task", 4096, (void * const)&lcdPrintedText, 15, NULL);
}
