#pragma once

#include <stdint.h>
#define TAG "app"

// LCD1602
#define LCD_NUM_ROWS               2
#define LCD_NUM_COLUMNS            32
#define LCD_NUM_VISIBLE_COLUMNS    16

// LCD2004
//#define LCD_NUM_ROWS               4
//#define LCD_NUM_COLUMNS            40
//#define LCD_NUM_VISIBLE_COLUMNS    20

// Undefine USE_STDIN if no stdin is available (e.g. no USB UART) - a fixed delay will occur instead of a wait for a keypress.
#define USE_STDIN  1
//#undef USE_STDIN

#define I2C_MASTER_NUM           I2C_NUM_0
#define I2C_MASTER_TX_BUF_LEN    0                     // disabled
#define I2C_MASTER_RX_BUF_LEN    0                     // disabled
#define I2C_MASTER_FREQ_HZ       400000
#define I2C_MASTER_SDA_IO        CONFIG_I2C_MASTER_SDA
#define I2C_MASTER_SCL_IO        CONFIG_I2C_MASTER_SCL


#ifdef __cplusplus
extern "C" {
#endif

/* declarations go here */
i2c_lcd1602_info_t * lcd_info;
typedef struct{
    uint8_t row;      /*the row */
    uint8_t col;      /*the column */
    char str[20];
    } lcdPrintedText_t;

void initializeLcd();
void lcd1602_task(void * pvParameter);
void writeToLCD(uint8_t row,uint8_t col,char text[20]);

#ifdef __cplusplus
}
#endif

